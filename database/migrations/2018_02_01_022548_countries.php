<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Countries.
 *
 * @author  The scaffold-interface created at 2018-02-01 02:25:48am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Countries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('countries',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('name');
        
        $table->String('capital');

        $table->String('flag');

        $table->String('map');

        $table->String('currency');

        $table->double('scurrency_value');

        $table->String('description');
        
        /**
         * Foreignkeys section
         */
        
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('countries');
    }
}
