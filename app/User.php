<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	/**
     * country.
     *
     * @return  \Illuminate\Support\Collection;
     */
    public function countries()
    {
        return $this->belongsToMany('App\Country');
    }

    /**
     * Assign a country.
     *
     * @param  $country
     * @return  mixed
     */
    public function assignCountry($country)
    {
        return $this->countries()->attach($country);
    }
    /**
     * Remove a country.
     *
     * @param  $country
     * @return  mixed
     */
    public function removeCountry($country)
    {
        return $this->countries()->detach($country);
    }
}
