@extends('scaffold-interface.layouts.app')
@section('title','Show')
@section('content')

<section class="content">
    <h1>
        Show country
    </h1>
    <br>
    <a href='{!!url("country")!!}' class = 'btn btn-primary'><i class="fa fa-home"></i>Country Index</a>
    <br>
    <table class = 'table table-bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td> <b>name</b> </td>
                <td>{!!$country->name!!}</td>
            </tr>
            <tr>
                <td> <b>capital</b> </td>
                <td>{!!$country->capital!!}</td>
            </tr>
            <tr>
                <td> <b>flag</b> </td>
                <td><img height='50px' src='{!!$country->flag!!}' /></td>
            </tr>
            <tr>
                <td> <b>currency code</b> </td>
                <td>{!!$country->currency!!}</td>
            </tr>
            <tr>
                <td> <b>currency value</b> </td>
                <td>{!!$country->currency_value!!}</td>
            </tr>
            <tr>
                <td> <b>map</b> </td>
                <td><div class="mapouter"><div class="gmap_canvas"><a href="https://www.webdesign-muenchen-pb.de"></a><iframe width="600" height="500" id="gmap_canvas" src="{!!$country->map!!}" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div></div></td>
            </tr>
            
        </tbody>
    </table>
</section>
@endsection