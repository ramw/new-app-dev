@extends('scaffold-interface.layouts.app')
@section('title','Index')
@section('content')

<section class="content">
    <h1>
    Favourite Countries List
    </h1>
    <br>
    <table class = "table table-striped table-bordered table-hover" style = 'background:#fff'>
        <thead>
            <th>name</th>
            <th>description</th>
            <th>remove favourite</th>
        </thead>
        <tbody>
            @foreach($countries as $country) 
            <tr>
                <td>{!!$country->name!!}</td>
                <td>{!!$country->description!!}</td>
                <td>
				<a  title='click to remove favourite' href = '{!!url("country")!!}/{!!$country->id!!}/deletefavourite' class = 'viewEdit btn btn-primary btn-xs' >
                <i class="fa fa-heart active"></i>
                </a>
				</td>                
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $countries->render() !!}

</section>
@endsection