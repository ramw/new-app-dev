<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//country Routes
Route::group(['middleware'=> 'web'],function(){
  Route::resource('country','\App\Http\Controllers\CountryController');
  Route::post('country/{id}/update','\App\Http\Controllers\CountryController@update');
  Route::get('country/{id}/addfavourite','\App\Http\Controllers\CountryController@addfavourite');
  Route::get('country/{id}/deletefavourite','\App\Http\Controllers\CountryController@deletefavourite');
  Route::get('favourite','\App\Http\Controllers\CountryController@indexfavourite');
  Route::get('country/{id}/delete','\App\Http\Controllers\CountryController@destroy');
  Route::get('country/{id}/deleteMsg','\App\Http\Controllers\CountryController@DeleteMsg');
});
